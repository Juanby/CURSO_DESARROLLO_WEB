<!DOCTYPE html>
<html>
<head>
	<title>estructuras de Control en PHP</title>
</head>
<body>
<?php
     /*las estructuras de control en lenguaje de programacion 
     se utilizan para realizr operaciones por ejemplo : selectivas, repetetivas  y ambas
     las estructuras de control mas utilizadas son:

     * IF(selectiva)
     * FOR(repetetivas)
     * WHILE(repetetiva condicional)
     */
     $x = 0;
     $y = 10;
     $z = 5;
     if ($x == 0){
     	echo "La variable es CERO";

     }else{
     	echo "La variable no es CERO";
     }
     //Estructura For , ejemplo
     for ($i=0; $i<=10; $i++){
     	echo "<br />Los Numeros Naturales hasta el 10: " . $i;
     }

     $colores = array('blue', 'green', 'yellow',  'violet', 'white', 'black' , 'orange');
     
     for ($i=0; $i<6; $i++){
     	echo "<br />Elemento $i =[".$colores[$i] . "]";
     }
//funcion count(), sirve para contar los elementos de un vectos

     for ($i=0; $i<count ($colores); $i++){
     	echo "<br /><h2>Color <h1 style = 'color: $colores[$i]'>$i : $colores[$i]</h1></h2>";
     }


     /*$numeros = array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20);

     for ($i = 0; $i < = count($numeros); $i=$i+2){
     		echo "<br />Multiplos de 2:" . $numeros[$i];
     	
     }*/


?>

</body>
</html>